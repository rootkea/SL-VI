# SL-VI
Software Laboratory 6

# How to use:  
	$ sudo apt-get install git				#use OS specific command to install git
	$ git clone https://github.com/rootkea/SL-VI.git
	$ cd SL-VI

# Assignments Index
1. Implement Two Phase commit protocol in Distributed Databases  
	
	Usage :  
	
		$ cd 1
		$ gcc server.c -pthread -o server
		$ ./server 51123 3				#port and clients
		$ gcc client.c -o client			#on other machine or separate tab
		$ ./client 127.0.0.1 51123			#substitute any LAN ip and port

2. [WIP]Implement Web Page ranking algorithm  

3. [WIP]Implement any one Partitioning technique in Parallel Databases  

4. Implement k-means Clustering method for the set of points into k
	district sets. Assume set of a point more than 20 and values of k is at
	least 3.  
	
	Usage :  
	
		$ cd 4
		$ gcc kmeans.c -lm -o kmeans
		$ ./kmeans < in.txt					#change values in in.txt
