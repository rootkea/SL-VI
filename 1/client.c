/*
*	Author: Avinash Sonawane <rootkea@gmail.com>
*	Title : Implement following operations using Socket Programming & Multithreading.
*			a) Addition of digits of a given number. (Ex 12345 = 15).
*			b) Find given number is prime or not.
*	License: GPLv3 or later
*/

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

void error(char *msg)
{
	perror(msg);
	exit(1);
}

int main(int argc, char **argv)
{
	int sockfd;
	struct sockaddr_in server;
	char buf[21];

	sockfd = socket(AF_INET, SOCK_STREAM, 0);
	if (sockfd == -1)
		error("socket() failed");

	server.sin_family = AF_INET;
	server.sin_port = htons(atoi(argv[2]));
	if(!inet_aton(argv[1], &server.sin_addr))
		error("Invalid Address");

	if (connect(sockfd, (const struct sockaddr *)&server, sizeof(server)) < 0)
		error("connect() failed");

	memset(buf, '\0', sizeof(buf));
	if (recv(sockfd, buf, sizeof(buf), 0) == -1)				// Commit?
		error("recv failed");
	printf("Coordinator : %s\n", buf);

	printf("Attempting transaction...\n");
	sleep(2);
	printf("Writing to undo log and redo log\n");
	sleep(1);
	
	printf("Was transaction successful? : ");
	scanf("%s", buf);											// Yes/No
	if (send(sockfd, buf, strlen(buf), 0) == -1)
		error("send failed");

	memset(buf, '\0', sizeof(buf));
	if (recv(sockfd, buf, sizeof(buf), 0) == -1)				// Commit/Rollback
		error("recv failed");
	printf("Coordinator : %s\n", buf);

	if (!strcmp(buf, "Commit"))
	{
		printf("Completing transaction...\n");
		sleep(1);
		printf("Locks released. Transaction complete.\n");
	}
	else
	{
		printf("Undoing transaction...\n");
		sleep(1);
		printf("Transaction undone.\n");
	}

	if (send(sockfd, "Done", strlen(buf), 0) == -1)			// Acknowledgement
		error("send failed");
	printf("Acknowledgemnt sent.\n");

	close(sockfd);

	return 0;
}
