/*
*	Author: Avinash Sonawane <rootkea@gmail.com>
*	Title : Implement following operations using Socket Programming & Multithreading.
*			a) Addition of digits of a given number. (Ex 12345 = 15).
*			b) Find given number is prime or not.
*	License: GPLv3 or later
*/

#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/ip.h>
#include <stdlib.h>
#include <string.h>
#include <pthread.h>
#include <unistd.h>

struct client
{
	int i;
	pthread_t thread;
	int newsockfd;
	char message[11];
};

void error(char *msg)
{
	perror(msg);
	exit(1);
}

void *phase1(void *argp)
{
	struct client *ptr = argp;
	char buf[21] = {'\0'};	

	printf("Sending query to commit to Cohort#%d\n", ptr->i);
	if (send(ptr->newsockfd, "Commit?", strlen("Commit?"), 0) == -1)
		error("send failed");

	if (recv(ptr->newsockfd, buf, sizeof(buf), 0) == -1)
		error("recv failed");
	
	strcpy(ptr->message, buf);
}

void *phase2(void *argp)
{
	struct client *ptr = argp;
	char buf[21] = {'\0'};	

	printf("Sending Consensus to Cohort#%d\n", ptr->i);
	if (send(ptr->newsockfd, ptr->message, strlen(ptr->message), 0) == -1)
		error("send failed");

	if (recv(ptr->newsockfd, buf, sizeof(buf), 0) == -1)
		error("recv failed");

	strcpy(ptr->message, buf);
}

int main(int argc, char **argv)
{
	int sockfd, i, n = atoi(argv[2]);
	char decision[11];
	struct client clients[n];
	struct sockaddr_in server;

	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == -1)
		error("socket() failed");

	server.sin_family = AF_INET;
	server.sin_port = htons(atoi(argv[1]));
	server.sin_addr.s_addr = INADDR_ANY;
	if (bind(sockfd, (const struct sockaddr *)&server, sizeof(server)) == -1)
		error("bind failed");
	
	if (listen(sockfd, 5) == -1)
		error("listen failed");

	printf("Waiting for cohorts...\n");
	for(i = 0; i < n; i++)
	{
		clients[i].i = i;
		if ((clients[i].newsockfd = accept(sockfd, NULL, NULL)) == -1)
			error("accept failed");
		printf("Cohort #%d connected.\n", i);
	}
	close(sockfd);
	printf("Cluster alive.\n");

	for(i = 0; i < n; i++)
		if(pthread_create(&(clients[i].thread), NULL, phase1, &clients[i]))		//phase1 starts
			error("pthread_create failed");

	for(i = 0; i < n; i++)
		if(pthread_join(clients[i].thread, NULL))								//Phase 1 complete
			error("pthread_join failed");

	strcpy(decision, "Commit");
	for(i = 0; i < n; i++)
	{
		if(!strcmp(clients[i].message, "No"))
		{
			strcpy(decision, "Rollback");
			break;
		}
	}
	for(i = 0; i < n; i++)
		strcpy(clients[i].message, decision);

	for(i = 0; i < n; i++)
		if(pthread_create(&(clients[i].thread), NULL, phase2, &clients[i]))		//Phase 2 Starts
			error("pthread_create failed");

	for(i = 0; i < n; i++)
	{
		if(pthread_join(clients[i].thread, NULL))								//Phase 2 complete
			error("pthread_join failed");
		printf("Client #%d : %s\n", i, clients[i].message);						// Acknowledgements
		close(clients[i].newsockfd);
	}

	if(!strcmp(decision, "Commit"))
		printf("Transaction complete.\n");
	else
		printf("Transaction undone.\n");;

	return 0;
}
